﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIExample.DTOs.Athlete
{
    public class AthleteCoachDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Coach { get; set; }
    }
}
