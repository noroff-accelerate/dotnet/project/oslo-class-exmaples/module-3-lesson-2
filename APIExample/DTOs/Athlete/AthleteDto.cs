﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIExample.DTOs.Athlete
{
    public class AthleteDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public int Records { get; set; }
        public int CoachId { get; set; }
    }
}
