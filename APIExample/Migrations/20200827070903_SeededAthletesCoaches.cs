﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace APIExample.Migrations
{
    public partial class SeededAthletesCoaches : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Coaches",
                columns: new[] { "Id", "Awards", "DOB", "Gender", "Name" },
                values: new object[,]
                {
                    { 1, 10, new DateTime(1980, 8, 27, 9, 9, 3, 642, DateTimeKind.Local).AddTicks(564), "Male", "John McIntyre" },
                    { 2, 15, new DateTime(1990, 8, 27, 9, 9, 3, 645, DateTimeKind.Local).AddTicks(161), "Female", "Renate Blindheim" },
                    { 3, 52, new DateTime(1946, 8, 27, 9, 9, 3, 645, DateTimeKind.Local).AddTicks(251), "Male", "Phil Jackson" },
                    { 4, 38, new DateTime(1985, 8, 27, 9, 9, 3, 645, DateTimeKind.Local).AddTicks(274), "Female", "Christine Girard" }
                });

            migrationBuilder.InsertData(
                table: "Athletes",
                columns: new[] { "Id", "CoachId", "DOB", "Gender", "Name", "Records" },
                values: new object[,]
                {
                    { 4, 1, new DateTime(1975, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), "Male", "David Beckham", 52 },
                    { 5, 1, new DateTime(1986, 8, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "Male", "Usain Bolt", 42 },
                    { 3, 2, new DateTime(1971, 9, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), "Male", "Kjetil Andre Aamodt", 28 },
                    { 1, 3, new DateTime(1963, 2, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), "Male", "Micheal Jordan", 10 },
                    { 2, 4, new DateTime(1971, 10, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "Male", "Thomas Ulsrud", 15 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Athletes",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Athletes",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Athletes",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Athletes",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Athletes",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Coaches",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Coaches",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Coaches",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Coaches",
                keyColumn: "Id",
                keyValue: 4);
        }
    }
}
