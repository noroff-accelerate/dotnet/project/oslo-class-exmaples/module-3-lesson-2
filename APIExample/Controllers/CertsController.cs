﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIExample.Models;

namespace APIExample.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CertsController : ControllerBase
    {
        private readonly TrainingDbContext _context;

        public CertsController(TrainingDbContext context)
        {
            _context = context;
        }

        // GET: api/Certs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Cert>>> GetCerts()
        {
            return await _context.Certs.ToListAsync();
        }

        // GET: api/Certs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Cert>> GetCert(int id)
        {
            var cert = await _context.Certs.FindAsync(id);

            if (cert == null)
            {
                return NotFound();
            }

            return cert;
        }

        // PUT: api/Certs/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCert(int id, Cert cert)
        {
            if (id != cert.Id)
            {
                return BadRequest();
            }

            _context.Entry(cert).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CertExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Certs
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Cert>> PostCert(Cert cert)
        {
            _context.Certs.Add(cert);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCert", new { id = cert.Id }, cert);
        }

        // DELETE: api/Certs/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Cert>> DeleteCert(int id)
        {
            var cert = await _context.Certs.FindAsync(id);
            if (cert == null)
            {
                return NotFound();
            }

            _context.Certs.Remove(cert);
            await _context.SaveChangesAsync();

            return cert;
        }

        private bool CertExists(int id)
        {
            return _context.Certs.Any(e => e.Id == id);
        }
    }
}
