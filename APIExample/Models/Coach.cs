﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace APIExample.Models
{
    public class Coach
    {
        // PK
        [Required]
        [Key]
        public int Id { get; set; }

        // Fields
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        public DateTime DOB { get; set; }
        [StringLength(50)]
        public string Gender { get; set; }
        public int Awards { get; set; }

        ICollection<CoachCert> CoachCerts { get; set; }

    }
}
