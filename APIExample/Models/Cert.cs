﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APIExample.Models
{
    public class Cert
    {
        public int Id { get; set; }
        public string Name { get; set; }

        ICollection<CoachCert> CoachCerts { get; set; }
    }
}
