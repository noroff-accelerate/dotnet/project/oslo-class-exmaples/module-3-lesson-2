﻿using APIExample.DTOs.Athlete;
using APIExample.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIExample.Profiles
{
    public class AthleteProfile : Profile
    {
        public AthleteProfile()
        {
            CreateMap<Athlete, AthleteDto>().ReverseMap();
            CreateMap<Athlete, AthleteListDto>();
            CreateMap<Athlete, AthleteCoachDto>().ForMember(acdto => acdto.Coach, opt => opt.MapFrom(a=>a.Coach.Name));
        }
    }
}
